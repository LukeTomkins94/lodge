


function initMap(){
  var map = new google.maps.Map(document.getElementById('map'), {

            center:{lat:51.481583,lng:-3.179090},
            zoom: 13,
          });

          var marker = new google.maps.Marker({
            position:{lat:51.481583,lng:-3.179090},
            map:map,
          })

          var searchBox = new google.maps.places.SearchBox(document.getElementById('mapsearch'));
          //mapsearch.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

          google.maps.event.addListener(searchBox, 'places_changed',function(){
            var places = searchBox.getPlaces();

            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for (i = 0; place=places[i]; i++) {
              bounds.extend(place.geometry.location);
              marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(15);
          })

          infoWindow = new google.maps.InfoWindow;

            if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        infoWindow.setPosition(pos);
        infoWindow.setContent('Location found.');
        infoWindow.open(map);
        map.setCenter(pos);
      }, function() {
        handleLocationError(true, infoWindow, map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  }

  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');  


};
